FROM registry.gitlab.com/buildgrid/buildbox/buildbox-e2e:latest

RUN apt-get update && apt-get install -y \
    docker.io \
    python3-pip

# Install chrootbuilder
RUN git clone --depth 1 https://gitlab.com/BuildGrid/buildbox/buildbox-tools.git && \
cd buildbox-tools/python/chrootbuilder && pip3 install .

# Pull specifc docker image so digests don't change
ENV DOCK_IMG "alpine@sha256:b276d875eeed9c7d3f1cfa7edb06b22ed22b14219a7d67c52c56612330348239"

ENV RECC_BINARY="/buildbox-e2e-build/recc/build/bin/recc"
ENV CASUPLOAD_BINARY="/buildbox-e2e-build/buildbox-tools/cpp/build/casupload/casupload"

# Construct depdencies to be uploaded
RUN mkdir -p /dependency/tmp /dependency/project_root && \
touch /dependency/project_root/dep.txt && \
echo "#!/bin/sh\necho \$TEST_COMMAND_STATUS" > /dependency/project_root/export.sh && \
chmod +x /dependency/project_root/export.sh

# test merge logic
RUN touch /dependency/tmp/tmp.txt; touch /dependency/tmp/extra.txt

RUN mkdir /test
WORKDIR /test

# RECC ENV Variables
ENV RECC_SERVER "controller:50051"
ENV RECC_INSTANCE ""
ENV RECC_FORCE_REMOTE 1
ENV RECC_PREFIX_MAP="/alpine=/:/dependency=/"
ENV RECC_REMOTE_ENV_TEST_COMMAND_STATUS="COMMAND EXECUTED SUCCCESSFULY!"
ENV RECC_REMOTE_ENV_PATH="/bin"
ENV RECC_OUTPUT_DIRECTORIES_OVERRIDE="project_root"
ENV RECC_DEPS_DIRECTORY_OVERRIDE="/dependency"

# pull alpine image, build alpine chroot, upload chroot, run command on userchroot
CMD docker pull $DOCK_IMG && chrootbuilder --extract $DOCK_IMG /alpine && \
$CASUPLOAD_BINARY --cas-server=http://$RECC_SERVER /alpine && \
$RECC_BINARY /project_root/export.sh | grep -w "COMMAND EXECUTED SUCCCESSFULY!" && \
test -e project_root/dep.txt
